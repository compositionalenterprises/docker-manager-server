FROM mono:latest
MAINTAINER Andrew Cziryak <andrewcz@andrewcz.com>

ARG MANAGER_SERVER_VERSION=19.5.13
ENV MANAGER_SERVER_PORT 8080
ENV MANAGER_SERVER_DATA_PATH /data

RUN apt-get update && apt-get install -y wget unzip && \
        mkdir /usr/share/manager-server /data && \
        wget https://github.com/Manager-io/Manager.zip/releases/download/${MANAGER_SERVER_VERSION}/Manager.zip -O /tmp/Manager.zip && \
        unzip /tmp/Manager.zip -d /usr/share/manager-server && \
        rm -rf /tmp/Manager.zip

EXPOSE ${MANAGER_SERVER_PORT}
VOLUME ${MANAGER_SERVER_DATA_PATH}

CMD /usr/bin/mono /usr/share/manager-server/ManagerServer.exe -port ${MANAGER_SERVER_PORT} -path ${MANAGER_SERVER_DATA_PATH}
