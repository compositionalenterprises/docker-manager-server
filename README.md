# docker-manager-server
Dockerized http://manager.io accounting software

View the list of versions here, or the latest version with the following command:

```bash
$ curl -sL https://www.manager.io/releases/ | head -n 1 | sed -e 's/^.*<table//g;s/\/table>.*$//g' | grep -Eo "[[:digit:]]{1,3}\.[[:digit:]]{1,3}\.[[:digit:]]{1,3}" | head -n 1
```

Run with
```
docker run -v $(pwd)/data:/data -p 8080:8080 smacz/manager-server
```

Open your browser http://dockerhost:8080. The default password for the user `administrator` is a blank password.

Backup could be done by:
```
docker run --rm --volumes-from managerio_data_1 \
  -v $(pwd):/backup smacz/manager-server \
  tar czvf /backup/Manager-backup.tar.gz /data
```
Restore:
```
 docker run --rm --volumes-from managerio_data_1 \
  -v $(pwd):/backup \ 
 smacz/manager-server bash -c "cd / && tar xzvf /backup/Manager-backup.tar.gz"
 ```
 Where managerio_data_1 is your data container.
